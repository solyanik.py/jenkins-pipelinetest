import unittest


class TestDictMethods(unittest.TestCase):

    def test_update(self):
        test_dict = {1: 'one'}
        test_dict.update({2: 'two'})
        result = {1: 'one', 2: 'two'}
        self.assertEqual(test_dict, result)

    def test_get(self):
        test_dict = {1: 'one', 2: 'two'}
        self.assertEqual(test_dict.get(2), 'two')
        self.assertEqual(test_dict.get(100), None)

    def test_pop(self):
        test_dict = {1: 'one', 2: 'two', 3: 'three'}
        test_dict_pop = test_dict.pop(2)
        self.assertEqual(test_dict, {1: 'one', 3: 'three'})
        self.assertEqual(test_dict_pop, 'two')

    def test_keys(self):
        test_dict = {1: 'one', 2: 'two', 3: 'three'}
        test_dict_keys = list(test_dict.keys())
        self.assertEqual(test_dict_keys, [1, 2, 3])

    def test_values(self):
        test_dict = {1: 'one', 2: 'two', 3: 'three'}
        test_dict_values = list(test_dict.values())
        self.assertEqual(test_dict_values, ['one', 'two', 'three'])

    def test_items(self):
        test_dict = {1: 'one', 2: 'two', 3: 'three'}
        test_dict_items = list(test_dict.items())
        self.assertEqual(test_dict_items, [(1, 'one'), (2, 'two'), (3, 'three')])


if __name__ == '__main__':
    unittest.main()
