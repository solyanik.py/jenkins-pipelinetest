python_version:
	python --version


tests:
	echo test python dictionary
	python3 task_01.py -v
	echo test minimum_bribes function
	python3 task_02_unittests.py -v
	echo test CSVConverter
	python3 task_03_unittests.py -v