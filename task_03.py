import csv
import json
import os

from sys import platform
from datetime import datetime


class CSVConverter:
    """Class that convert CSV files to JSON for linux OS
    and to txt for Windows OS"""
    supported_os = ['linux', 'linux2', 'win32']

    def __init__(self, csv_file_path, output_folder_path):
        self.platform = platform
        self.csv_file_path = csv_file_path
        self.output_folder_path = output_folder_path

    def read_csv(self):
        """Method that read CSV file
        and add values to dictionary"""
        if CSVConverter.validate_os(self):
            try:
                with open(self.csv_file_path) as csv_file:
                    csv_reader = csv.DictReader(csv_file)
                    temperature_statistic = {"temperature_statistic": [
                        {
                            'datetime': CSVConverter.convert_timestamp_to_date(row['timestamp']),
                            'celsius_temp': float(row['temperature']),
                            'fahrenheit_temp': CSVConverter.convert_celsius_to_fahrenheit(row['temperature']),
                            'kelvin_temp': CSVConverter.convert_celsius_to_kelvin(row['temperature'])
                        }
                        for row in csv_reader if CSVConverter.validate_data(row)]}
                    return temperature_statistic
            except FileNotFoundError:
                return False

    def write_file(self):
        """Method that write JSON or txt file
        depends on user OS"""

        if not CSVConverter.read_csv(self):
            return 'Path to input file not found.'

        try:
            with open(CSVConverter.set_os_output_file_format(self.output_folder_path,
                                                             self.csv_file_path), 'w') as file:
                file.write(json.dumps(CSVConverter.read_csv(self), indent=4))
            return 'File successfully created'
        except FileNotFoundError:
            return 'Path to output folder not found'

    def validate_os(self):
        """Method to validate your OS"""
        if self.platform not in CSVConverter.supported_os:
            return 'Your OS not supported'
        return True

    @staticmethod
    def set_os_output_file_format(folder, csv_file):
        """Method that set output file format
        JSON - linux
        Windows - txt"""
        filename = os.path.splitext(os.path.basename(csv_file))[0]
        if platform == 'linux' or platform == 'linux2':
            return folder + filename + '.json'
        elif platform == 'win32':
            return folder + filename + '.txt'

    @staticmethod
    def validate_data(row):
        """Method that validate is values in CSV file
        correct and can be converted"""

        try:
            date = float(row['timestamp'])
            datetime.utcfromtimestamp(date)
        except ValueError:
            print(f"Not accepted timestamp value: {row['timestamp']}")
            return False
        except OverflowError:
            print(f"Timestamp out of range: {row['timestamp']}")
            return False
        except OSError:
            print(f"Timestamp value too large: {row['timestamp']}")
            return False

        try:
            if float(row['temperature']) < -273.13:
                print(f"Not accepted temperature value: {row['temperature']}")
                return False
        except ValueError:
            print(f"Not accepted temperature value: {row['temperature']}")
            return False

        return True

    @staticmethod
    def convert_timestamp_to_date(timestamp):
        """Method that convert timestamp to dat"""
        date = str(datetime.utcfromtimestamp(float(timestamp)).strftime('%m/%d/%YT%-I:%M%P'))
        return date

    @staticmethod
    def convert_celsius_to_fahrenheit(temperature):
        """Methods that convert celsius temperature to fahrenheit"""
        fahrenheit = round((float(temperature) * 9 / 5) + 32, 2)
        return fahrenheit

    @staticmethod
    def convert_celsius_to_kelvin(temperature):
        """Method that convert celsius temperature to kelvin"""
        kelvin = round(float(temperature) + 273.15, 2)
        return kelvin


if __name__ == '__main__':
    csv_temp = CSVConverter('/var/jenkins_home/workspace/functests/temperature.csv',
                            '/var/jenkins_home/workspace/functests/')
    print(csv_temp.write_file())
