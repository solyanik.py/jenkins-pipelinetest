import unittest

from task_02 import minimum_bribes


class TestMinimumBribes(unittest.TestCase):

    def test_one_bribe(self):
        self.assertEqual(minimum_bribes([2, 1]), 1)

    def test_no_bribes(self):
        self.assertEqual(minimum_bribes([1, 2, 3, 4, 5]), 0)

    def test_count_bribes(self):
        self.assertEqual(minimum_bribes([3, 2, 5, 1, 2]), 6)

    def test_chaotic_queue(self):
        self.assertEqual(minimum_bribes([4, 2, 3, 5]), 'Too chaotic')


if __name__ == '__main__':
    unittest.main()
