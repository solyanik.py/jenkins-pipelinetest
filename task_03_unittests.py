import unittest
import sys
import os

from task_03 import CSVConverter


class TestCSVConverter(unittest.TestCase):

    def test_path_to_input_file_does_not_exist(self):
        csv_test_input_file_error = CSVConverter(
            '/var/jenkins_home/workspace/functests/error.csv',
            '/var/jenkins_home/workspace/functests/')
        self.assertEqual(csv_test_input_file_error.write_file(), 'Path to input file not found.')

    def test_path_to_output_folder_does_not_exist(self):
        csv_test_output_folder_error = CSVConverter(
            '/var/jenkins_home/workspace/functests/temperature.csv',
            '/var/jenkins_home/workspace/functests/error/path/')
        self.assertEqual(csv_test_output_folder_error.write_file(), 'Path to output folder not found')

    def test_path_as_environment_variable(self):
        os.environ['CSV_TEST_PATH_ENVIRON_PATH'] = \
            '/var/jenkins_home/workspace/functests/temperature.csv'
        os.environ['CSV_TEST_FOLDER_ENVIRON_PATH'] = '/var/jenkins_home/workspace/functests/'
        csv_test_env_var_path = CSVConverter(os.environ.get('CSV_TEST_PATH_ENVIRON_PATH'),
                                             os.environ.get('CSV_TEST_FOLDER_ENVIRON_PATH'))
        self.assertEqual(csv_test_env_var_path.write_file(), 'File successfully created')

    def test_incorrect_values_in_csv_file(self):
        test_timestamp_value_error = {'timestamp': 'error_value', 'temperature': 20}
        test_timestamp_overflow_error = {'timestamp': 5555555555555555555555555, 'temperature': 30}
        test_temperature_value_error = {'timestamp': 870911700, 'temperature': 'error_value'}
        test_temperature_not_accepted_value = {'timestamp': 1370641582, 'temperature': -280}
        self.assertEqual(CSVConverter.validate_data(test_timestamp_value_error), False)
        self.assertEqual(CSVConverter.validate_data(test_timestamp_overflow_error), False)
        self.assertEqual(CSVConverter.validate_data(test_temperature_value_error), False)
        self.assertEqual(CSVConverter.validate_data(test_temperature_not_accepted_value), False)

    @unittest.skipIf(sys.platform.startswith("win"), 'Does not make sense for non linux os')
    def test_create_json_file(self):
        test_create_json_file_for_linux = CSVConverter(
            '/var/jenkins_home/workspace/functests/temperature.csv',
            '/var/jenkins_home/workspace/functests/')
        directory_files = os.listdir('/var/jenkins_home/workspace/functests/')
        self.assertEqual('temperature.json' in directory_files, True)
        self.assertEqual(test_create_json_file_for_linux.write_file(), 'File successfully created')

    @unittest.skipUnless(sys.platform.startswith("win"), "requires Windows")
    def test_create_txt_file(self):
        test_create_txt_file_for_windows = CSVConverter(r'C:\Users\Bohdan\temperature.csv',
                                                        r'C:\Users\Bohdan')
        directory_files = os.listdir(r'C:\Users\Bohdan')
        self.assertEqual('temperature.txt' in directory_files, True)
        self.assertEqual(test_create_txt_file_for_windows.write_file(), 'File successfully created')


if __name__ == '__main__':
    unittest.main()
