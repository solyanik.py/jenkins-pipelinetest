def minimum_bribes(queue):
    bribes = 0

    for idx, el in enumerate(queue, start=1):
        if el - idx > 2:
            return 'Too chaotic'

        for potential_briber in queue[max(0, el - 3):idx]:
            if potential_briber > el:
                bribes += 1
    return bribes
